<?php
/**
 *
 */
class Clientes extends CI_Controller
{

  function __construct()
  {
    // code...
    parent::__construct();
    $this->load->model('Cliente');
  }
  public function index(){      // $this->load->view('header');
  		// $this->load->view('instructores/index');
  		// $this->load->view('footer');
      $data['clientes']=$this->Cliente->obtenerTodos();
      $this->load->view('header');
      $this->load->view('clientes/index',$data);
      $this->load->view('footer');
    }

  public function nuevo(){
    $this->load->view('header');
    $this->load->view('clientes/nuevo');

  }

  public function guardar(){
    $datosNuevoCliente=array(
      'cedula_cli'=>$this->input->post('cedula_cli'),
      'nombre_cli'=>$this->input->post('nombre_cli'),
      'apellido_cli'=>$this->input->post('apellido_cli'),
      'correo_cli'=>$this->input->post('correo_cli'),
      'celular_cli'=>$this->input->post('celular_cli'),
      'direccion_cli'=>$this->input->post('direccion_cli'),
    );

          if ($this->Cliente->insertar($datosNuevoCliente)) {
            redirect('clientes/index');

            // code...
          } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
            // code...
          }




        }
        //funcion para eliminar Instructores
        public function eliminar($id_cli){
          if ($this->Cliente->borrar($id_cli)) {
            redirect('clientes/index');
            // code...
          } else {
            echo "ERROR AL BORRAR";
            // code...
          }


        }

}//cierre

 ?>
