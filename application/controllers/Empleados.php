<?php
/**
 *
 */
class Empleados extends CI_Controller
{

  function __construct()
  {
    // code...
    parent::__construct();
    $this->load->model('Empleado');
  }
  public function index(){      // $this->load->view('header');
  		// $this->load->view('instructores/index');
  		// $this->load->view('footer');
      $data['empleados']=$this->Empleado->obtenerTodos();
      $this->load->view('header');
      $this->load->view('empleados/index',$data);
      $this->load->view('footer');
    }

  public function nuevo(){
    $this->load->view('header');
    $this->load->view('empleados/nuevo');

  }

  public function guardar(){
    $datosNuevoEmpleado=array(
      'cedula_emp'=>$this->input->post('cedula_emp'),
      'nombre_emp'=>$this->input->post('nombre_emp'),
      'apellido_emp'=>$this->input->post('apellido_emp'),
      'correo_emp'=>$this->input->post('correo_emp'),
      'celular_emp'=>$this->input->post('celular_emp'),
      'direccion_emp'=>$this->input->post('direccion_emp'),
      'area_emp'=>$this->input->post('area_emp'),
    );

          if ($this->Empleado->insertar($datosNuevoEmpleado)) {
            redirect('empleados/index');

            // code...
          } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
            // code...
          }




        }
        //funcion para eliminar Instructores
        public function eliminar($id_emp){
          if ($this->Empleado->borrar($id_emp)) {
            redirect('empleados/index');
            // code...
          } else {
            echo "ERROR AL BORRAR";
            // code...
          }


        }






}//cierre

 ?>
