<?php
/**
 *
 */
class Casas extends CI_Controller
{

  function __construct()
  {
    // code...
    parent::__construct();
    $this->load->model('Casa');
  }
  public function index(){      // $this->load->view('header');
  		// $this->load->view('instructores/index');
  		// $this->load->view('footer');
      $data['casas']=$this->Casa->obtenerTodos();
      $this->load->view('header');
      $this->load->view('casas/index',$data);
      $this->load->view('footer');
    }

  public function nuevo(){
    $this->load->view('header');
    $this->load->view('casas/nuevo');
    $this->load->view('footer');


  }

  public function guardar(){
    $datosNuevoCasa=array(
      'color_c'=>$this->input->post('color_c'),
      'tamano_c'=>$this->input->post('tamano_c'),
      'precio_c'=>$this->input->post('precio_c'),
      'ubicacion_c'=>$this->input->post('ubicacion_c'),
    );

          if ($this->Casa->insertar($datosNuevoCasa)) {
            redirect('casas/index');

            // code...
          } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
            // code...
          }




        }
        //funcion para eliminar Instructores
        public function eliminar($id_c){
          if ($this->Casa->borrar($id_c)) {
            redirect('casas/index');
            // code...
          } else {
            echo "ERROR AL BORRAR";
            // code...
          }


        }

}//cierre

 ?>
