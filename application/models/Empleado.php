<?php
  class Empleado extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datosNuevoEmpleado){
        return $this->db->insert("empleado",$datosNuevoEmpleado);
    }
//Funcion para consultar instructores
    function obtenerTodos(){
      $listadoEmpleados=
      $this->db->get("empleado");
      if ($listadoEmpleados->num_rows()>0) {//si jay datos
          return $listadoEmpleados->result();// code...
      } else {
        // code...
        return false;
      }

    }
    //BORRAR INSTRUCTORERS
    function borrar($id_emp){
      //delete from instructor where id_ins=1;
      $this->db->where("id_emp",$id_emp);
      return $this->db->delete("empleado");
      //if ($this->db->delete("instructor")) {
        //return TRUE;
        // code...
      //} else {
        //return FALSE;
        // code...
      }


  }//Cierre de la clase

 ?>
