<div class="row">
  <div class="container text-center">
    <img src="https://www.bellacollina.com/hubfs/Cam%20Bradford%20Header%202.jpg" width="1200px" height="300px" alt="">

  </div>
  ;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

  <div class="col-md-12">
    <h1 class="text-center" style="background-color:powderblue">LISTADO DE CASAS</h1>

  </div>
  <div class="col-md-10">

  </div>

  <div class="col-md-2">
    <a href="<?php echo site_url('casas/nuevo'); ?>" <i class="glyphicon glyphicon-plus" ?></i> Nueva Casa</a>

  </div>

</div>

<br>
<?php if ($casas): ?>
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>COLOR</th>
        <th>TAMAÑO</th>
        <th>PRECIO</th>
        <th>UBICACIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($casas as $filaTemporal): ?>
        <tr>
          <td>
              <?php echo $filaTemporal->id_c; ?>
          </td>
          <td>
              <?php echo $filaTemporal->color_c; ?>
          </td>
          <td>
              <?php echo $filaTemporal->tamano_c; ?>
          </td>
          <td>
              <?php echo $filaTemporal->precio_c; ?>
          </td>
          <td>
              <?php echo $filaTemporal->ubicacion_c; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Casa">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url();?>/casas/eliminar/<?php echo $filaTemporal->id_c;?>" title="Eliminar Cliente" onclick="return confirm('¿Esta seguro?');">
              <i class="glyphicon glyphicon-trash" style="color:red"></i>
            </a>
          </td>

        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>


<?php else: ?>
  <h1>No hay datos</h1>
<?php endif; ?>
