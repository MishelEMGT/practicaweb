<div class="row">
  <div class="col-md-12">
    <h1 class="text-center" style="background-color:powderblue">LISTADO DE EMPLEADO</h1>

  </div>
  <div class="container text-center">
    <img src="https://img.freepik.com/vector-premium/trabajar-icono-conjunto-equipos-empleado-subordinado-engranaje-tarea-distribucion-conferencia-linea-flecha-concepto-negocio-infografia-linea-tiempo-iconos-4-pasos-icono-linea-vector-negocios_579956-3121.jpg?w=2000" width="1200px" height="300px" alt="">

  </div>
  <div class="col-md-10">

  </div>

  <div class="col-md-2">
    <a href="<?php echo site_url('empleados/nuevo'); ?>" <i class="glyphicon glyphicon-plus" ?></i> Nuevo Empleado</a>

  </div>
  ;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

</div>
<br>
<?php if ($empleados): ?>
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>CÉDULA</th>
        <th>NOMBRES</th>
        <th>APELLIDOS</th>
        <th>CORREO</th>
        <th>CELULAR</th>
        <th>DIRECCIÓN</th>
        <th>ÁREA</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($empleados as $filaTemporal): ?>
        <tr>
          <td>
              <?php echo $filaTemporal->id_emp; ?>
          </td>
          <td>
              <?php echo $filaTemporal->cedula_emp; ?>
          </td>
          <td>
              <?php echo $filaTemporal->nombre_emp; ?>
          </td>
          <td>
              <?php echo $filaTemporal->apellido_emp; ?>
          </td>
          <td>
              <?php echo $filaTemporal->correo_emp; ?>
          </td>
          <td>
              <?php echo $filaTemporal->celular_emp; ?>
          </td>
          <td>
              <?php echo $filaTemporal->direccion_emp; ?>
          </td>
          <td>
              <?php echo $filaTemporal->area_emp; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Empleado">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url();?>/empleados/eliminar/<?php echo $filaTemporal->id_emp;?>" title="Eliminar Empleado" onclick="return confirm('¿Esta seguro?');" >
              <i class="glyphicon glyphicon-trash" style="color:red"></i>
            </a>
          </td>

        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>


<?php else: ?>
  <h1>No hay datos</h1>
<?php endif; ?>
