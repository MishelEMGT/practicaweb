<div class="row">
  <div class="col-md-12">
    <h1 class="text-center" style="background-color:powderblue">LISTADO DE CLIENTES</h1>

  </div>
  <div class="container text-center">
    <img src="https://res.cloudinary.com/dte7upwcr/image/upload/blog/blog2/atencion-al-cliente-online/atencion-al-cliente-online-img_header.jpg" width="1200px" height="300px" alt="">

  </div>
  <div class="col-md-10">

  </div>

  <div class="col-md-2">
    <a href="<?php echo site_url('clientes/nuevo'); ?>" <i class="glyphicon glyphicon-plus" ?></i> Nuevo Cliente</a>

  </div>
  ;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

</div>
<br>
<?php if ($clientes): ?>
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>CÉDULA</th>
        <th>NOMBRES</th>
        <th>APELLIDOS</th>
        <th>CORREO</th>
        <th>CELULAR</th>
        <th>DIRECCIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($clientes as $filaTemporal): ?>
        <tr>
          <td>
              <?php echo $filaTemporal->id_cli; ?>
          </td>
          <td>
              <?php echo $filaTemporal->cedula_cli; ?>
          </td>
          <td>
              <?php echo $filaTemporal->nombre_cli; ?>
          </td>
          <td>
              <?php echo $filaTemporal->apellido_cli; ?>
          </td>
          <td>
              <?php echo $filaTemporal->correo_cli; ?>
          </td>
          <td>
              <?php echo $filaTemporal->celular_cli; ?>
          </td>
          <td>
              <?php echo $filaTemporal->direccion_cli; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Cliente">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url();?>/clientes/eliminar/<?php echo $filaTemporal->id_cli;?>" title="Eliminar Cliente" onclick="return confirm('¿Esta seguro?');">
              <i class="glyphicon glyphicon-trash" style="color:red"></i>
            </a>
          </td>

        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>


<?php else: ?>
  <h1>No hay datos</h1>
<?php endif; ?>
